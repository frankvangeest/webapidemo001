﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp001.Data
{
    public class Game
    {
        [Key]
        public string id { get; set; }

        public string description { get; set; }
    }
}
